#!/usr/bin/perl

package PAPIDev;

use Data::Dumper;

use PAPIDev::Parser::JSON;
use PAPIDev::Parser::PCSV;
use PAPIDev::Processor;
use PAPIDev::Generator;

sub __json_highlight {

    my $parser = shift;

    if ($parser->status_detail() =~ /offset (\d+)/) {

    open my $fh, '/tmp/dump.json';

    my $offset = 0;
    my $found = 0;

        while (my $line = readline $fh) {
            print $line;
            if (length($line) + $offset > $1 and not $found) {
                print (('-' x ($1 - $offset - 1)) . '^' . "\n");
                $found = 1;
            }

            $offset += length $line;
        }
    }
}

sub main {

    my $parser;
    my ($res, $already_parsed);

    my $opt = shift @ARGV;

    if ($opt =~ /--json/) {
        $parser = new PAPIDev::Parser::JSON;
    } elsif ($opt =~ /--pscv/) {
        $parser = new PAPIDev::Parser::PCSV;
    } elsif ($opt =~ /--dump/) {

        my $objtype = shift @ARGV;

        print "Dumping json to /tmp/dump.json file...\n";
        `Util/chrootexec.sh $objtype > /tmp/dump.json`;

        if ((shift @ARGV or '') =~ /-e/) {

            ! -z '/tmp/dump.json' or
                die "File /tmp/dump.json is empty";

            my $editor = $ENV{EDITOR} || 'vim';
            system $editor => '/tmp/dump.json';
            $parser = new PAPIDev::Parser::JSON;

            until ($parser->parse('/tmp/dump.json')) {

                system("clear");

                $parser->print_error();
                __json_highlight($parser);

                readline;
                system $editor => '/tmp/dump.json';
            }
        } else {
            return 0;
        }
    }
        
    my $path = shift @ARGV || '/tmp/dump.json';

    my $res = $parser->parse($path)
        or die $parser->print_error();

    my $processor = new PAPIDev::Processor;
    my $pres = $processor->process($res);

    my $gen = new PAPIDev::Generator;

    $gen->print($pres, $_);
}

main();


1;
