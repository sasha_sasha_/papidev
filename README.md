## Usage

### Example

#### JSON

```bash
$ perl PAPIDev.pm --json '[{"object": {"object": "Infoblox::Grid::RestartBannerSetting", "ibap": "restart_banner_setting", "struct": 1}, "members": [{"member": "enable", "ibap": "enabled", "type": "boolean", "simple": 1}]}]'
```

#### PseudoCSV

```bash
$ perl PAPIDev.pm --pcsv "
> HEADER-object,ibap,opts,gopts
> Infoblox::Grid::RestartBannerSetting,restart_banner_setting,(struct=y),(simple=y)
> HEADER-member,type,opts
> enable,boolean,(ibap=enabled)"
```

#### JSON with --dump key

```bash
# dump json description to /tmp/dump.json
$ perl PAPIDev.pm --dump one.Restart_Banner_Setting

# edit /tmp/dump.json file (edit '"member": "enabled"'
# line to '"member": "enabled"
# or specify -e option in previous command to run editor
# $perl PAPIDev.pm --dump one.Restart_Banner_Setting -e

# by default use /tmp/dump.json
$ perl PAPIDev.pm --json | sed 's/, }/}/'
```

### Output

```perl
package Infoblox::Grid::RestartBannerSetting;

use strict;
use Carp;

use Infoblox::Util;
use Infoblox::IBAPBase;
use Infoblox::IBAPTypes;
use Infoblox::PAPIOverrides;

use vars qw(
            $_object_type
            %_allowed_members
            %_ibap_to_object
            %_name_mappings
            %_object_to_ibap
            %_reverse_name_mappings
            @ISA
            @_return_fields
);

@ISA = qw( Infoblox::IBAPBase Infoblox::PAPIOverrides::ALL );

BEGIN {

    $_object_type = 'Restart_Banner_Setting';
    register_wsdl_type('ib:Restart_Banner_Setting', 'Infoblox::Grid::RestartBannerSetting');


    %_allowed_members = (
                         'enable' => {simple => 'bool', validator => \&ibap_value_o2i_boolean},
    );

    Infoblox::IBAPBase::create_accessors(\%_allowed_members);

    %_name_mappings = (
                       'enable' => 'enabled',
    );

    %_reverse_name_mappings = reverse %_name_mappings;

    %_ibap_to_object = (
    );

    %_object_to_ibap = (
                        'enable' => \&ibap_o2i_boolean,
    );

    @_return_fields = (
                       tField('enabled'),
    );
}


sub __new__ {

    my ($proto, %args) = @_;
    my $class = ref($proto) || $proto;
    my $self = Infoblox::IBAPBase->__new__(%args);

    bless $self, $class;
    return $self;
}

sub new {

    my ($proto, %args) = @_;
    my $class = ref($proto) || $proto;
    my $self = Infoblox::IBAPBase->new(%args);

    bless $self, $class;

    if (!$self->__initialize_members_from_arg_list__(\%_allowed_members, \%args) ||
        !$self->__validate_object_required_members__(\%_allowed_members))
    {
        return;
    }

    return $self;
}

sub __ibap_object_type__ {

    return $_object_type;
}

# ----
# o2i
# ----

# ----
# i2o
# ----

# ----------------------------
# non-autogenerated accessors
# ----------------------------


1;
```
