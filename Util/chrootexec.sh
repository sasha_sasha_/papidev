#!/bin/bash

sudo cp Util/gather.py $CHR/tmp/gather.py

sudo chroot $CHR /bin/bash -x <<EOF 2> /dev/null
    python /tmp/gather.py $1
EOF
