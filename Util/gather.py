import os
import sys
import json

import infoblox.one.admin_conn.objtype as ot
import infoblox.one.onetypes as onet
import infoblox.one.extensible_attribute_definition as ead

IBAP_PREFIX, IBAP_CLASS = sys.argv[1].split('.')

IBAP_PATH = ('/infoblox/{0}/lib/python'
        '/infoblox/{0}/ibap'.format(IBAP_PREFIX))

for cmd in (
    'grep -rl \'class {0}(\' {1}',
    'grep -rl \'struct *({0}\' {1}',
):
    PATH = os.popen(cmd.format(IBAP_CLASS, IBAP_PATH)).read()
    if PATH: break

IBAP_MODULE = 'infoblox.{1}.ibap.{0}'.format(
    os.path.basename(os.path.splitext(PATH)[0]),
        IBAP_PREFIX, )

BASEOBJ_FILTER = [
   'access',
   'inherited_extensible_attributes',
   'multi_edit_members',
   'object_id',
   'readfield_substitution',
   'smart_folder',
   'task_objs',
   'uuid',
   'extensible_attributes',
]

TYPE_MAPPING = {
    'string': 'string',
    'unsignedInt': 'uint',
    'boolean': 'boolean',
    'IPAddress': 'ipaddr',
    'dateTime': 'datetime',
    'BaseObject': 'id',
}

def _type(t):

    if t in TYPE_MAPPING:
        return TYPE_MAPPING[t]
    else:
        return 'struct'

obj = __import__(IBAP_MODULE,
    fromlist=[IBAP_CLASS]).__dict__[IBAP_CLASS]

members = []

res = {
    'object': {
        'object': 'Infoblox::{0}'.format(IBAP_CLASS),
        'ibap': IBAP_CLASS,
    },
    'members': members
}

if IBAP_CLASS in ead.USER_ALLOWED_OBJECT_TYPES:
    res['object']['eas'] = 1

if '_' in IBAP_CLASS:
    res['object']['struct'] = 1
    res['object']['ibap'] = IBAP_CLASS.lower()


for mmbr in obj.members:
    m = {}

    if mmbr.name in BASEOBJ_FILTER: continue

    m['ibap'] = m['member'] = mmbr.name

    if mmbr.ro or 'noupdate' in obj.__dict__:
        m['mode'] = 'ro'

    if mmbr.undisclosed:
        m['mode'] = 'wo'

    if mmbr.req and m.get('mode', None) != 'ro':
        m['required'] = 1

    tname = mmbr.tname.split(':')[-1]

    if tname.startswith('ArrayOf'):
            m['array'] = 1

    tname = tname.split('ArrayOf')[-1]

    m['type'] = _type(tname)

    if m['type'] == 'struct':
        m['validator'] = [tname]

    if isinstance(mmbr, ot.TEnum):
        m['enum'] = mmbr.choices
        m['type'] = 'string'
    elif isinstance(mmbr.ofwhat, ot.TEnum):
        m['enum'] = mmbr.ofwhat.choices
        m['type'] = 'string'
        del m['validator']

    if 'certificate' in mmbr.name and 'data_ref' in mmbr.tname:
        m['type'] = 'cert_dataref'
        m['mode'] = 'wo'
        del m['validator']

    members.append(m)

print json.dumps([res], indent=4)
