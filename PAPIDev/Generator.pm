package PAPIDev::Generator;

use strict;
use Template;

use PAPIDev::Loader;

our $_MAIN_TEMPLATE = 'PAPIDev/Templates/main.tt';

sub new {

    my $cls = shift;

    my $self = {
        'loader' => new PAPIDev::Loader,
    };

    bless $self, $cls;
    return $self;
}

sub loader {
    my $self = shift;
    return $self->{loader};
}

sub __print {

    my ($self, $processed, $type) = @_;

    if (ref $type eq 'CODE') {
        printf $type->();
        return;
    }

    my $members = $$processed{$type};
    my $temp = new Template(RECURSION => 1);

    printf "    " . $self->loader->get_varsyms($type) . "_$type = (\n";
    $temp->process($self->loader->get_template_names($type), {
        form_indent  => sub { return (' ' x shift) },
        indent       => $self->loader->get_indents($type),
        members      => $members,
    });
    printf("    );\n\n");
}

sub print {

    my ($self, $processed) = @_;

    foreach my $proc (@$processed) {

        my $temp = new Template(RECURSION => 1);
        $temp->process($_MAIN_TEMPLATE, {
            object       => $$proc{object},
            form_indent  => sub { return (' ' x shift) },
            get_members  => sub { my $type = shift; $$proc{members}{$type} },
            get_template => sub { $self->loader->get_template_names(shift) },
            get_indent   => sub { $self->loader->get_indents(shift) },
            get_varsym   => sub { $self->loader->get_varsyms(shift) },
        });
    }
}

1;
