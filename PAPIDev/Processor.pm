#!/usr/bin/perl

package PAPIDev::Processor;

use strict;
use PAPIDev::Loader;

sub new {

    my $cls = shift;

    my $self = {
        'ctx'    => {},
        'loader' => new PAPIDev::Loader,
    };

    bless $self, $cls;
    return $self;
}

sub ctx {
    my $self = shift;
    return $self->{ctx};
}

sub loader {
    my $self = shift;
    return $self->{loader};
}

sub __get_longest_key {

    my ($self, $array, $key) = @_;
    my $longest;

    foreach my $e (@$array) {
        $longest = $e->{$key} if length $e->{$key} > length $longest;
    }

    return $longest;
}

sub __process_aux {

    my ($self, $parsed, $type) = @_;

    my $members = $$parsed{'members'};
    my $sortkey = $self->loader->get_sortkeys($type);
    my $mapping = $self->loader->get_handlers($type);

    my @newm;

    foreach my $m (@$members) {
        my $newm = {%{$self->loader->get_defaults($type)}};
        
        foreach (keys %$mapping) {
            $self->{ctx}->{__coderef} = $$mapping{$_};
            $$mapping{$_}->($newm, $m, $self->ctx());
        }

        push @newm, $newm unless $$newm{__skip};
    }

    my $longest = $self->__get_longest_key(\@newm, $sortkey);
    $_->{indent} = length($longest) - length($_->{$sortkey})
        foreach @newm;

    return [sort {$a->{$sortkey} cmp $b->{$sortkey}} @newm];
}

sub __process_object {

    my ($self, $parsed, $type) = @_;

    my $obj     = $$parsed{object};
    my $mapping = $self->loader->get_handlers('object');
    my $newobj  = {%{$self->loader->get_defaults('object')}};

    foreach (keys %$mapping) {
        $self->{ctx}->{__coderef} = $$mapping{$_};
        $$mapping{$_}->($newobj, $obj, $self->ctx());
    }

    return $newobj;
}

sub process {
    
    my ($self, $parsed) = @_;
    my %res;

    my @result;

    foreach my $descr (@$parsed) {
        my %res;

        $$self{ctx}{__parsed} = $descr;

        my $obj = $self->__process_object($descr, 'object');

        foreach my $k (keys %{$self->loader->get_handlers()}) {
            $res{$k} = $self->__process_aux($descr, $k) if $k ne 'object';
        }

        push @result, {object => {%$obj, %{$$self{ctx}}}, members => \%res};

        delete $$self{ctx}{__parsed};
    }

    return \@result;
}


1;
