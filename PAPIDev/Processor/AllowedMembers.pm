package PAPIDev::Processor::AllowedMembers;

use strict;
use PAPIDev::Processor::Base;

our $_TYPE          = 'allowed_members';
our $_TEMPLATE_NAME = 'allowed_members.tt';
our $_SORTKEY       = 'name';
our $_INDENT        = 25;
our $_VARSYM        = '%';

our %_DEFAULTS = (
    skip_accessor => 0,
    mandatory     => 0,
    array         => 0,
    simple        => 0,
    readonly      => 0,
    writeonly     => 0,
    use           => undef,
    name          => undef,
    use_truefalse => 0,
    use_members   => undef,
    validator     => undef,
    enum          => undef,
    special       => undef,
    nomixed       => 0,
);

our %_allowed_members_validator_mapping = (
    'boolean'      => 'ibap_value_o2i_boolean',
    'string'       => 'ibap_value_o2i_string',
    'uint'         => 'ibap_value_o2i_uint',
    'int'          => 'ibap_value_o2i_int',
    'name'         => 'ibap_value_o2i_string',
    'ipv4addr'     => 'ibap_value_o2i_ipv4addr',
    'ipv6addr'     => 'ibap_value_o2i_ipv6addr',
    'ipaddr'       => 'ibap_value_o2i_ipaddr',
    'datetime'     => 'ibap_value_o2i_uint',
    'cert_dataref' => 'ibap_value_o2i_string',
);

our %_allowed_members_simple_mapping = (
    'boolean' => 'bool',
    'string'  => 'asis',
    'uint'    => 'asis',
    'int'     => 'asis',
);

our %_KEY_HANDLERS = (

    'member'   => __copy('name', 'member'),
    'type'     => __copy('type'),
    'enum'     => __copy('enum'),
    'special'  => __copy('special'),
    'required' => __yn('mandatory', 'required'),
    'nomixed'  => __yn('nomixed'),
    'array'    => __yn('array'),

    'ibap' => sub {
        my ($to, $from, $ctx) = @_;
        $$to{ibap} = $$from{ibap} || $$from{member};
    },

    'mode' => sub {
        my ($to, $from, $ctx) = @_;
        $$to{readonly} = 1 if $$from{'mode'} =~ /ro/;
        $$to{writeonly} = 1 if $$from{'mode'} =~ /wo/ or $$from{type} eq 'cert_dataref';
    },

    'validator' => sub {
        my ($to, $from, $ctx) = @_;
        return if defined $$from{enum} or $$from{mode} =~ /ro/;
        $$to{validator} = $$from{validator} ||
            $_allowed_members_validator_mapping{$$from{type}};
    },

    'simple' => sub {
        my ($to, $from, $ctx) = @_;

        if (not $$from{i2o}) {
            $$to{simple} = $_allowed_members_simple_mapping{$$from{type}};
            #XXX: Determine whether this condition is more usefull
            #if __is_true($$from{simple});
        }
    },

    skip_accessor => sub {
        my ($to, $from, $ctx) = @_;
        $$to{skip_accessor} = $$from{skip_accessor};

        #put this in context for further generation
        #of custom accessors
        $$ctx{'custom_accessor'}{$$from{member}} = 1
            if $$to{skip_accessor};
        },
);


1;
