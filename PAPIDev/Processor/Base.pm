package PAPIDev::Processor::Base;

require Exporter;
use strict;

our @ISA = ('Exporter');

our @EXPORT = (
    '__yn',
    '__copy',
    '__is_true',
    'get_defaults',
    'get_key_handlers',
    'get_type',
    'get_template_name',
    'get_sortkey',
    'get_indent',
    'get_varsym',
);

sub __yn {

    my ($to, $from) = @_;

    $from = $to unless $from;

    return sub {
        my ($toobj, $fromobj, $ctx) = @_;
        $$toobj{$to} = __is_true($$fromobj{$from});
    };
}

sub __copy {

    my ($to, $from) = @_;

    $from = $to unless $from;

    return sub {
        my ($toobj, $fromobj, $ctx) = @_;
        $$toobj{$to} = $$fromobj{$from};
    };
}

sub __is_true { return (shift =~ /^(y|1$)/i ? 1 : 0) }

sub get_defaults {

    my $cls = shift;

    {
        no strict 'refs';
        return \%{$cls . '::_DEFAULTS'};
    }
}

sub get_key_handlers {

    my $cls = shift;

    {
        no strict 'refs';
        return \%{$cls . '::_KEY_HANDLERS'};
    }
}

sub get_type {

    my $cls = shift;

    {
        no strict 'refs';
        return ${$cls . '::_TYPE'};
    }
}

sub get_template_name {

    my $cls = shift;

    {
        no strict 'refs';
        return ${$cls . '::_TEMPLATE_NAME'};
    }
}

sub get_sortkey {

    my $cls = shift;

    {
        no strict 'refs';
        return ${$cls . '::_SORTKEY'};
    }
}

sub get_indent {

    my $cls = shift;

    {
        no strict 'refs';
        return ${$cls . '::_INDENT'};
    }
}

sub get_varsym {

    my $cls = shift;

    {
        no strict 'refs';
        return ${$cls . '::_VARSYM'};
    }
}


1;
