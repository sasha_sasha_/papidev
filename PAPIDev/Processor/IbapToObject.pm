package PAPIDev::Processor::IbapToObject;

use strict;
use PAPIDev::Processor::Base;

our $_TYPE          = 'ibap_to_object';
our $_TEMPLATE_NAME = 'o2i_i2o.tt';
our $_SORTKEY       = 'name';
our $_INDENT        = 24;
our $_VARSYM        = '%';

our %_DEFAULTS = (
    'name'     => undef,
    'function' => undef,
);

our %_function_mapping = (
    'struct'      => 'ibap_i2o_generic_object_convert',
    'object'      => 'ibap_i2o_generic_object_convert',
    'id'          => 'ibap_i2o_generic_object_convert',
    'name'        => 'ibap_i2o_object_name',
    'datetime'    => 'ibap_i2o_datetime_to_unix_timestamp',
);

our %_function_array_mapping = (
    'struct' => 'ibap_i2o_generic_object_list_convert',
    'name'   => 'ibap_i2o_object_list_names',
    'object' => 'ibap_i2o_generic_object_list_convert',
);

our %_writeonly_types = (
    'cert_dataref' => 1,
);

our %_KEY_HANDLERS = (

    'member'   => sub {
        my ($to, $from, $ctx) = @_;
        $$to{name} = $$from{ibap} || $$from{member};
    },
    'i2o'      => sub {
        my ($to, $from, $ctx) = @_;

        if (__is_true($$from{array})) {
            $$to{function} = $_function_array_mapping{$$from{i2o}} ||
                $$from{i2o} || $_function_array_mapping{$$from{type}};
        } else {
            $$to{function} = $_function_mapping{$$from{i2o}}
                || $$from{i2o} || $_function_mapping{$$from{type}};
        }

        my $name = $$from{ibap} || $$from{member};

        $$ctx{'default_i2o'}{$name} = 1
            if $$from{type} eq 'boolean';

        $$ctx{'default_i2o_arr'}{$$from{member}} = 1
            if __is_true($$from{array});

        $$ctx{'use_i2o'}{$$from{member}} = $name
            if __is_true($$from{use_flag});

        if (
            __is_true($$from{simple})       or
            not $$to{function}              or
            $_writeonly_types{$$from{type}}
        ) {
            $$to{__skip} = 1;
            return;
        }

        #put this in context for further generation
        #of custom i2o
        $$ctx{'custom_i2o'}{$$from{member}} = $$to{function}
            if $$to{function} =~ /^__\w+__$/;
    },
);


1;
