package PAPIDev::Processor::ReturnFieldOverrides;

use strict;
use PAPIDev::Processor::Base;

our $_TYPE          = 'return_field_overrides';
our $_TEMPLATE_NAME = 'return_field_overrides.tt';
our $_SORTKEY       = 'name';
our $_INDENT        = 32;
our $_VARSYM        = '%';

our %_DEFAULTS = (
    'name'      => undef,
    'overrides' => [],
);

our %_writeonly_types = (
    'cert_dataref' => 1,
);

our %_KEY_HANDLERS = (

    'member'   => sub {
        my ($to, $from, $ctx) = @_;
        $$to{name} = $$from{member};
    },
    'overrides' => sub {
        my ($to, $from, $ctx) = @_;

        if (
            $$from{mode} =~ /wo/            or
            $_writeonly_types{$$from{type}}
        ) {
            $$to{__skip} = 1;
            return;
        }

        $$to{overrides} = $$from{overrides} || [];
    },
);


1;
