package PAPIDev::Processor::ReturnFields;

use strict;
use PAPIDev::Processor::Base;

our $_TYPE          = 'return_fields';
our $_TEMPLATE_NAME = 'return_fields.tt';
our $_SORTKEY       = 'name';
our $_INDENT        = '23';
our $_VARSYM        = '@';

our %_DEFAULTS = (
    'name'          => undef,
    'return_fields' => undef,
    'return_flags'  => undef,
    'funccall'      => undef,
);

sub __return_fields_aux {

    my ($from, $ctx) = @_;

    if (ref $from eq 'HASH') {
        if ($$from{name}) {
            return {
                name          => $$from{name},
                return_flags  => $$from{rflags},
                return_fields => __return_fields_aux($$from{rfields}, $ctx),
            };
        } else {
            return __return_fields_aux($$from{rfields}, $ctx);
        }
    } elsif (ref $from eq 'ARRAY') {
        return [map { __return_fields_aux($_, $ctx) } @$from];
    } else {
        return {
            'name' => $from,
        } if $from;
    }
}

our %_writeonly_types = (
    'cert_dataref' => 1,
);

our %_KEY_HANDLERS = (

    'return_fields' => sub {
        my ($to, $from, $ctx) = @_;

        if (
            $$from{mode} =~ /wo/            or
            $_writeonly_types{$$from{type}}
        ) {
            $$to{__skip} = 1;
            return;
        }

        my $name = $$from{ibap} || $$from{member};

        if (not $$from{rfields} and ref $$from{validator} eq 'ARRAY') {

            if (grep {$_ =~ /Infoblox::/} @{$$from{validator}}) {
                $$ctx{instance_constants}{$name} = $$from{validator}[0];

                $$ctx{instance_constants_longest} = length($name)
                    if $$ctx{instance_constants_longest} < length($name);

                $$ctx{init_instance_constants} = 1;
                $$to{__skip} = 1;
                return;
            }
        }

        if (ref $$from{rfields} eq 'HASH') {
            $$to{name} = $$from{rfields}{name} || $$from{ibap} || $$from{member};
            $$to{funccall} = $$from{rfields}{funccall};
            $$to{return_flags} = $$from{rfields}{rflags};

            if (__is_true($$from{rfields}{skip})) {
                $$to{__skip} = 1;
                return;
            }

            if ($$from{rfields}{funccall} and __is_true($$from{rfields}{skipname})) {
                $$to{name} = '~';
                $$to{skipname} = 1;
                return;
            }

            $$to{return_fields} = __return_fields_aux($$from{rfields}, $ctx)
                if $$from{rfields};

        } else {
            $$to{name} = $$from{ibap} || $$from{member};
            $$to{return_fields} = __return_fields_aux($from, $ctx);
        }

        if (not $$from{rfields}) {
            if ($$from{type} eq 'name') {
                $$to{return_fields} = [{'name' => 'name'}];
            }
        }
    },
);


1;
