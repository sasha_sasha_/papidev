package PAPIDev::Processor::NameMappings;

use strict;
use PAPIDev::Processor::Base;

our $_TYPE          = 'name_mappings';
our $_TEMPLATE_NAME = 'name_mappings.tt';
our $_SORTKEY       = 'name';
our $_INDENT        = 23;
our $_VARSYM        = '%';

our %_DEFAULTS = (
    'name' => undef,
    'ibap' => undef,
);

our %_KEY_HANDLERS = (
    'member' => __copy('name', 'member'),

    'ibap'   => sub {
        my ($to, $from, $ctx) = @_;
        if (not $$from{ibap} or $$from{ibap} eq $$from{member}) {
            $$to{__skip} = 1;
            return;
        }
        $$to{ibap} = $$from{ibap};
    },
);


1;
