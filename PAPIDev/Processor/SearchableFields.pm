package PAPIDev::Processor::SearchableFields;

use strict;
use PAPIDev::Processor::Base;

our $_TYPE          = 'searchable_fields';
our $_TEMPLATE_NAME = 'searchable_fields.tt';
our $_SORTKEY       = 'name';
our $_INDENT        = 27;
our $_VARSYM        = '%';


our %_DEFAULTS = (
    'name'     => undef,
    'o2i'      => undef,
    'type'     => 'REGEX',
    'ibap'     => undef,
);

my %_function_mapping = (
    'string'      => 'ibap_o2i_string',
    'int'         => 'ibap_o2i_int',
    'uint'        => 'ibap_o2i_uint',
    'boolean'     => 'ibap_o2i_boolean',
    'struct'      => 'ibap_o2i_generic_struct_convert',
    'struct_list' => 'ibap_o2i_generic_struct_list_convert',
    'id'          => 'ibap_o2i_object_by_oid_only',
    'id_or_name'  => 'ibap_o2i_object_by_oid_or_name',
    'datetime'    => 'ibap_o2i_unix_timestamp_to_datetime',
);

our %_KEY_HANDLERS = (
    'member'   => __copy('name', 'member'),
    'ibap'     => sub {

        my ($to, $from, $ctx) = @_;
        $$to{ibap} = $$from{search}{ibap} || $$from{ibap} || $$from{member};

    },

    'type'     => sub {

        my ($to, $from, $ctx) = @_;
        $$to{type} = $$from{search}{type};
    },

    'o2i'      => sub {

        my ($to, $from, $ctx) = @_;

        if (not $$from{search} or 
            (ref $$from{search} eq 'HASH'
                and not keys %{$$from{search}})
        ) {
            $$to{__skip} = 1;
            return;
        }

        $$to{function} = $_function_mapping{$$from{search}{o2i}} || 
                         $$from{search}{o2i}                     ||
                         $_function_mapping{$$from{o2i}}         ||
                         $$from{o2i}                             ||
                         $_function_mapping{$$from{type}};

        #put this in context for further generation
        #of custom search o2i
        $$ctx{'__custom_search_o2i'}{$$from{member}} = $$to{function}
            if $$to{function} =~ /__/;
    },
);


1;
