package PAPIDev::Processor::Object;

use strict;
use PAPIDev::Processor::Base;

our $_TYPE          = 'object';

our %_DEFAULTS = (
    name    => undef,
    ibap    => undef,
    session => undef,
    eas     => 0,
);

our @_EA_EXTATTRS = (
    {
        'member'        => 'extattrs',
        'special'       => 'extensible_attributes',
        'ibap'          => 'extensible_attributes',
        'i2o'           => 'Infoblox::IBAPBase::__i2o_extensible_attributes__',
        'o2i'           => 'Infoblox::IBAPBase::__o2i_extensible_attributes__',
        'search'        => {'type' => 'SUBSET'},
        'rfields'       => {'funccall' => 'return_field_extensible_attributes', skipname => 'yes'},
    },
    {
        'member'  => 'extensible_attributes',
        'special' => 'extensible_attributes',
        'o2i'     => 'ibap_o2i_ignore',
        'search'  => {'type' => 'SUBSET', 'o2i' => 'Infoblox::IBAPBase::__o2i_extensible_attributes__'},
        'rfields' => {skip => 'yes'},
    },
);



our %_KEY_HANDLERS = (

    'object'   => __copy('name', 'object'),

    'ibap' => sub {
        my ($to, $from, $ctx) = @_;
        $$to{ibap} = $$from{ibap} || $$from{object};
    },

    'eas' => sub {

        my ($to, $from, $ctx) = @_;

        unshift @{$ctx->{__parsed}{members}}, @_EA_EXTATTRS
            if __is_true($$from{eas});

        $$to{eas} = __is_true($$from{eas});
    },

    'struct' => __yn('struct'),

    'papioverrides' => sub {

        my ($to, $from, $ctx) = @_;

        if (__is_true($$from{struct})) {
            $$to{papioverrides} = 'ALL';
        } else {
            $$to{papioverrides} = uc $$from{papioverrides};
        }
    }
);


1;
