package PAPIDev::Processor::ObjectToIbap;

use strict;
use PAPIDev::Processor::Base;

our $_TYPE          = 'object_to_ibap';
our $_TEMPLATE_NAME = 'o2i_i2o.tt';
our $_SORTKEY       = 'name';
our $_INDENT        = 24;
our $_VARSYM        = '%';


our %_DEFAULTS = (
    'name'     => undef,
    'function' => undef,
);

my %_function_mapping = (
    'string'       => 'ibap_o2i_string',
    'int'          => 'ibap_o2i_int',
    'uint'         => 'ibap_o2i_uint',
    'boolean'      => 'ibap_o2i_boolean',
    'struct'       => 'ibap_o2i_generic_struct_convert',
    'id'           => 'ibap_o2i_object_by_oid_only',
    'id_or_name'   => 'ibap_o2i_object_by_oid_or_name',
    'datetime'     => 'ibap_o2i_unix_timestamp_to_datetime',
    'cert_dataref' => 'ibap_o2i_cert_data_ref',
    'object'       => 'ibap_o2i_object_by_oid_only',
);

my %_function_array_mapping = (
    'struct' => 'ibap_o2i_generic_struct_list_convert',
    'object' => 'ibap_o2i_object_by_oid_only',
);


our %_KEY_HANDLERS = (
    'member'   => __copy('name', 'member'),

    'o2i'      => sub {

        my ($to, $from, $ctx) = @_;

        if ($$from{'mode'} =~ /ro/) {
            $$to{function} = 'ibap_o2i_ignore';
            return;
        }

        if (__is_true($$from{array})) {
            $$to{function} = $_function_array_mapping{$$from{o2i}} ||
                $$from{o2i} || $_function_array_mapping{$$from{type} . '_list'};
        } else {
            $$to{function} = $_function_mapping{$$from{o2i}} || 
                $$from{o2i} || $_function_mapping{$$from{type}};
        }

        #put this in context for further generation
        #of custom o2i
        $$ctx{'custom_o2i'}{$$from{member}} = $$to{function}
            if $$to{function} =~ /^__\w+__$/;
    },
);


1;
