package PAPIDev::Loader;

use strict;

our $_SINGLETONE;

sub new {

    unless ($_SINGLETONE) {

        my $cls = shift;
        my $self = {
            'HANDLERS'       => {},
            'DEFAULTS'       => {},
            'TEMPLATE_NAMES' => {},
            'SORTKEYS'       => {},
            'INDENTS'        => {},
            'VARSYMS'        => {},
        };

        $_SINGLETONE = bless $self, $cls;
        $self->__load();

        {
            no strict 'refs';

            foreach (keys %$self) {

                my $key = $_;
                my $method = 'PAPIDev::Loader::get_' . lc $_;
                *$method = sub {
                    my ($self, $type) = @_;
                    return $$self{$key}{$type} if $type;
                    return $$self{$key};
                };
            }
        }

        return $self;

    } else {
        return $_SINGLETONE;
    }
}

sub __load {

    #Load all required data from Processor directory

    my $self = shift;

    opendir DIR, 'PAPIDev/Processor' or
        die "cannot open dir 'PAPIDev/Processor': $!";

    my @plugins = readdir DIR;
    closedir DIR;

    foreach my $p (@plugins) {

        next if $p =~ /^\./ or $p =~ /^Base\.pm$/;

        (my $module = "PAPIDev::Processor::$p") =~ s/\.pm$//;
        my $file    = "PAPIDev/Processor/$p";

        require $file;

        my $type = $module->get_type();

        $$self{HANDLERS}{$type}       = $module->get_key_handlers();
        $$self{DEFAULTS}{$type}       = $module->get_defaults();
        $$self{TEMPLATE_NAMES}{$type} = 'PAPIDev/Templates/' . ($module->get_template_name());
        $$self{SORTKEYS}{$type}       = $module->get_sortkey();
        $$self{INDENTS}{$type}        = $module->get_indent();
        $$self{VARSYMS}{$type}        = $module->get_varsym();
    }
}


1;
