package PAPIDev::Parser::Base;

# ----------
# accessors
# ----------

sub parser { 

    my $self = shift;
    return $self->{parser};
}

sub preprocessor {

    my $self = shift;
    return $self->{preprocessor};
}

sub status {
    
    my $self = shift;
    
    if (@_) {
        $self->{status} = shift;
    } else { 
        return $self->{status}; 
    }
}

sub status_detail {
    
    my $self = shift;
    
    if (@_) {
        $self->{status_detail} = shift; 
    } else {
        return $self->{status_detail}; 
    }
}

sub set_error {
    
    my $self = shift;

    $self->status(shift);
    $self->status_detail(shift);
}

sub print_error {

    my $self = shift;
    print STDERR $self->status . ': ' . $self->status_detail . "\n";

} 

sub parse {

    # preprocesses and parses the value given
    # depending on whether it's a file or a string

    my ($self, $value) = @_;

    return (-f $value ? $self->parse_file($value) :
        $self->parse_line($value));
}

sub parse_file {
    die 'parse_file is not implemented';
}

sub parse_line {
    die 'parse_line is not implemented';
}


1;
