#!/usr/bin/perl

package PAPIDev::Parser::PCSV;

use strict;
use warnings;

use PAPIDev::Parser::PCSV::Preprocessor;
use PAPIDev::Parser::PCSV::Grammar;

our @ISA = ('PAPIDev::Parser::Base');

# ------------
# constructor
# ------------

sub new {

    my $class = shift;
    $class = ref $class || $class;

    my $preproc = new PAPIDev::Parser::PCSV::Preprocessor;

    my $self = {
        parser        => $PAPIDev::Parser::PCSV::Grammar::grammar,
        status        => '0',
        preprocessor  => $preproc,
        status_detail => '',
        @_,
    };

    bless $self, $class;
    return $self;
}

# ----------------
# parse functions
# ----------------

sub parse_file {

    # preprocesses and parses file given
    # and return nice hash/array representing the file

    my ($self, $path) = @_;

    $self->set_error(0, '');

    my $str = $self->preprocessor->preprocess_file($path);
    my @res = ($str =~ $self->parser);

    return ($self->_parse_check($str, $res[0]) ?
        $self->_minimize_tree(\%/) : 0);
}

sub parse_line {

    # preprocesses and parses line given
    # and return nice hash/array representing the string

    my ($self, $line) = @_;

    $self->set_error(0, '');

    my $str = $self->preprocessor->preprocess_line($line);
    my @res = ($str =~ $self->parser);

    return ($self->_parse_check($str, $res[0]) ?
        $self->_minimize_tree(\%/) : 0);
}

sub parse {

    my ($self, $value) = @_;

    my $res = $self->SUPER::parse($value);

    if ($res) {
        return $self->__join_members($self->__join_headers($res));
    } else {
        return $res;
    }
}

# -----------
# parse util
# -----------

sub _parse_check {

    # checks whether the whole line was parsed
    # if not ouputs unparsed chunk of the line

    my ($self, $str, $res) = @_;

    if ($res ne $str) {

        my $unparsed = substr($str, length $res) =~ s/\n//gmr;
        $self->set_error(255, 'Cannot parse \'' . $unparsed . '\'');
        return 0;

    } 

    return 1;
}

# --------------------------
# minimize result tree util
# --------------------------

sub _minimize_tree {

    my ($self, $res) = @_;
    return _minimize_tree_aux($res);
}

# minimization table
our %_minimize_mapping = (
    'list'          => \&_parse_list,
    'scalar'        => \&_parse_scalar,
    'keyvalue_list' => \&_parse_keyvalue_list,
    'lines'         => \&_parse_lines,
);

# auxillary
sub _minimize_tree_aux {

    my $res = shift;

    foreach (keys %_minimize_mapping) {
        return $_minimize_mapping{$_}->($res) if $$res{$_};
    } 
}

sub _parse_list {

    # list minimization routine

    my $res = shift;

    return (ref $$res{list} eq 'HASH' ?
        _minimize_tree_aux($$res{list}) :
        [map {_minimize_tree_aux($_)} @{$$res{list}}]
    );
}

sub _parse_scalar {

    # scalar minimization routine
    
    my $res = shift;
    return $$res{scalar} =~ s/#/ /gr;
}

sub _parse_lines {

    # line minimization routine
    
    my $res = shift;
    _minimize_tree_aux($$res{lines});
}

sub _parse_keyvalue_list { 

    # key/value pair list minimization

    my %res;

    foreach my $element (@{shift->{keyvalue_list}}) {
        $res{$element->{name}} = _minimize_tree_aux($element->{value});
    }

    return \%res;
}

# -----------
# join utils
# -----------

sub __join_headers {

    # join header section with according sections
    # to create HASH

    my ($self, $value) = @_;
    my ($current_hdr, @result);

    foreach my $val (@$value) {

        if (not ref $val->[0] and $val->[0] =~ s/HEADER-//) {
            $current_hdr = $val;
            next;
        }

        return 0 unless $current_hdr;

        my %tmp_res;
        $tmp_res{$$current_hdr[$_]} = $$val[$_] foreach 0 .. $#$val;
        push @result, \%tmp_res;
    }

    return \@result;
}

sub __join_members {

    # separate objects from each other

    my ($self, $value) = @_;
    my ($current_obj, $gopts, $objopts, $mopts, @result);

    foreach my $val (@$value) {

        if ($val->{object}) {

            push @result, $current_obj if $current_obj;

            $current_obj = {};

            $objopts = delete $$val{opts};
            $objopts = (ref $objopts ? $objopts : {});

            $gopts = delete $$val{gopts};
            $gopts = (ref $gopts ? $gopts : {});

            $$current_obj{object}  = { %$val, %$objopts };
            $$current_obj{members} = [];

            next;
        }

        $mopts = delete $$val{opts};
        $mopts = (ref $mopts ? $mopts : {});

        push @{$$current_obj{members}}, { %$gopts, %$mopts, %$val };
    }

    push @result, $current_obj;

    return \@result;
}


1;
