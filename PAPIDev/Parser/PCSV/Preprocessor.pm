package PAPIDev::Parser::PCSV::Preprocessor;

BEGIN {
    our $PREPROCESS_RE_FILE = [
        #     s1       s2    mod
        # ---------  ------ -----

        # Removes comments. Comment is a line
        # that starts with ';;'
        [ '^;;.*\n',  '',    'gm' ],

        # Remove whitespaces
        [ ' *',       '',    'gm' ], 

        # Replace multi-line break with one-line
        # break
        [ '\n{2,}',   '\n',  'gm' ],

        # Remove line-break after separator
        # (separator indicates that line is not finished)
        [ '(,|;)\n',  '$1',  'gm' ],

        # Remove line break after open bracket
        [ '\(\n',     '\(',  'gm' ],

        # Remove line break before close bracket
        [ '\n\)',     '\)',  'gm' ],

        # Remove last line-break
        [ '\n$',      '',    'm'  ],

        # Remove last separator from list
        # (it does not separates anything)
        [ '(,|;)\)',  '\)',  'gm' ],
    ];
    
    our $PREPROCESS_RE_CL = [
        #     s1       s2    mod
        # ---------  ------ -----

        # Replace twicely escaped slash with
        # Slash escaped once
        [   '\\n',    '\n',  'gm' ],
    ];
}
    
# ------------
# constructor
# ------------

sub new {

    my $class = shift;
    $class = ref $class || $class;

    bless {}, $class;
}

sub _apply_regexprs {

    my ($self, $str, $regs) = @_;

    foreach my $re (@$regs) {
        my ($s1, $s2, $m) = @$re;
        eval "\$str =~ s/$s1/$s2/$m";
    }

    return $str;
}

sub preprocess_file {

    my ($self, $file) = @_;
    open(my $fh, '<', $file);
    return $self->_apply_regexprs((join '', <$fh>),
        $PREPROCESS_RE_FILE);
}

sub preprocess_line {

    my ($self, $str) = @_;
    return $self->_apply_regexprs($str, $PREPROCESS_RE_CL);
}


1;
