package PAPIDev::Parser::PCSV::Grammar;

=head1 GRAMMAR

Formal rules:

 ----------------------------------------------------------

 <LINES>          := <LINE>\n<LINES> | <LINE>
 <LINE>           := <ELEMENT>,<LINE> | <ELEMENT>
 <ELEMENT>        := <LIST> | <SCALARSEQ>
 <SCALARSEQ>      := <SCALAR><SCALARSEQ> | <SCALAR>
 <LIST>           := ( <KEYVALUELIST> | <LIST_SCALAR> )
 <KEYVALUELIST>   := <SCALARSEQ>=<ELEMENT>;<KEYVALUELIST> | <SCALARSEQ>=<ELEMENT>
 <LIST_SCALARSEQ> := <LIST>;<LIST_SCALARSEQ> | <SCALARSEQ>;<LIST_SCALARSEQ> | <LIST> | <SCALARSEQ>
 <SCALAR>         := a | b | ... | z | 0 | ... | 9 | : | _ | - | # | ! | > | <

 ----------------------------------------------------------

  - grammar can consist of several <line>s;
  - each <line> consist of <element>s divided by a coma; 
  - each element on a <line> can be either <list> or <scalar>
  - <scalar> is a simple identifier that may contain only a-zA-Z0-9:_- characters
  - <list> is sequence of <keyvalue>s or <scalar>s/<list>s divided by ';' and
    surrounded by round brackets '(' and ')'.
  - <list> supports any level of deep (that perl can support while parsing result),
    and can contain only one of (<keyvalue> or <scalar>/<list>)
    values inside the list, eg 
    
        (a;b;c)             #correct
        (a;b;(c;d;e))       #correct
        (a=b;c=e)           #correct
        (a=b;c=d;e=(f;i;g)) #correct
        (a;b=c)             #incorrect / should be (a;(b=c))
        (a=b;(a=c;d=e))     #incorrect / should be (a=b;_list=(a=c;d=e))

 ----------------------------------------------------------

=cut

use Regexp::Grammars;

our $grammar = qr{

   <lines>
   <nocontext:>

   <rule: lines>       <[list=line]>+    % <.linesep>
   <rule: line>        <[list=element]>+ % <.sep>

   <rule: element>     <list> | <scalar>

   <rule: list>        \( 
                         (
                          <[keyvalue_list=keyvalue]>+ % <.listsep> |
                          <[list=element]>+           % <.listsep>
                         )
                       \)

   <rule: keyvalue>    <name=scalar>=<value=element>

   <rule: scalar>      [a-zA-Z0-9:_\-\#!><]+

   <rule: linesep>     \n
   <rule: sep>         ,
   <rule: listsep>     ;
};


1;
