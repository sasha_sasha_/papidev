package PAPIDev::Parser::JSON;

use strict;
use warnings;

use JSON;
use parent 'PAPIDev::Parser::Base';

# ------------
# constructor
# ------------

sub new {

    my $class = shift;
    $class = ref $class || $class;

    my $self = {
        'parser'      => new JSON,
        status        => '0',
        status_detail => '',
        @_,
    };

    bless $self, $class;
    return $self;
}

# ----------------
# parse functions
# ----------------

sub parse_file {

    my ($self, $file) = @_;
    my ($fh, $res);

    $self->set_error(0, '');

    unless (open($fh, '<', $file)) {
        $self->set_error('250', 'Cannot open file ' . $file . ': ' . $@);
        return 0;
    }

    eval {
        $res = $self->parser->decode(join '', <$fh>);
    };

    if ($@) {
        $self->set_error('255', 'JSON Error: ' . $@);
        return 0;
    }

    return $res;
}

sub parse_line {

    my ($self, $line) = @_;
    my $res;

    $self->set_error(0, '');

    eval { 
        $res = $self->parser->decode($line);
    };

    if ($@) {
        $self->set_error('255', 'JSON Error: ' . $@);
        return 0;
    }

    return $res;
}


1;
